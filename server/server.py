#$ curl -v -i http://127.0.0.1:8000/GET?key=abc
import time
from http.server import BaseHTTPRequestHandler
from urllib import parse
import os
import json
import simplejson #for reading put data as json object
import socket
import requests
import sys

PRIMARY_KV_STORE={}
BACKUP_KV_STORE={}
PRIMARY_RANGE=['','']
BKP_RANGE=['','']
active_slaves=0
MASTER_ADDR=[]
def print_server_data():
    global PRIMARY_KV_STORE
    global BACKUP_KV_STORE
    global PRIMARY_RANGE
    global BKP_RANGE
    global MASTER_ADDR
    print("----------------------------------------------------")
    print("                   SERVER DATA                      ")
    print("----------------------------------------------------")
    print("MASTER ADDRESS: ", MASTER_ADDR[0], MASTER_ADDR[1])
    print("PRIMARY KEY RANGE: ",PRIMARY_RANGE)
    print("BACKUP KEY RANGE: ",BKP_RANGE)
    print("PRIMARY DATA: ", PRIMARY_KV_STORE)
    print("BACKUP DATA: ",BACKUP_KV_STORE)
    print("----------------------------------------------------")

class handleRequests(BaseHTTPRequestHandler):

    #def __init__(self):
       

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        
    def do_GET(self):
        self._set_headers()
        parsed_path=parse.unquote(self.path)
        parsed_path = parse.urlparse(self.path)
        url_path=parsed_path.path
        query = parsed_path.query
        global node
        global PRIMARY_RANGE
        global BKP_RANGE
        global PRIMARY_KV_STORE
        global BACKUP_KV_STORE
        if url_path=="/master/update_meta":
            get_params=query.split("&")
            for get_param in get_params:
                temp=get_param.split("=")
                if temp[0]=="prim_range_start":
                    PRIMARY_RANGE[0]=temp[1]
                elif temp[0]=="prim_range_end":
                    PRIMARY_RANGE[1]=temp[1]
                elif temp[0]=="bkp_range_start":
                    BKP_RANGE[0]=temp[1]
                elif temp[0]=="bkp_range_end":
                    BKP_RANGE[1]=temp[1]
                    
                    
            self.wfile.write(bytes("Done","utf-8"))
            print("Updated Key Ranges:")
            print("Primary Key Range: ",PRIMARY_RANGE)
            print("Backup Key Range: ",BKP_RANGE)

        if url_path=="/pool_transfer/get_backup":
            print("Sending Back up data to pool slave")
            self.wfile.write(bytes(json.dumps(BACKUP_KV_STORE),'utf-8'))
            print("Back Up data sent.")

        if url_path=="/pool_transfer/get_prim":
            print("Sending Primary data to pool slave")
            self.wfile.write(bytes(json.dumps(PRIMARY_KV_STORE),'utf-8'))
            print("Primary data sent.")

        if '/search' == url_path:   ##get request for particular storage node
            search=query.split("=")
            search=search[1]
            send_ip,send_port=findStorageNode(search)
            send_data=[send_ip,send_port]
            if send_ip!=-1:
                self.wfile.write(bytes(json.dumps(send_data), 'utf-8'))
                return
            else:
                self.wfile.write(bytes("not found", 'utf-8'))
                return
        if '/search_backup' == url_path:   ##get request for particular storage node
            search=query.split("=")
            search=search[1]
            send_ip,send_port=findBackupNode(search)
            send_data=[send_ip,send_port]
            if send_ip!=-1:
                self.wfile.write(bytes(json.dumps(send_data), 'utf-8'))
                return
            else:
                self.wfile.write(bytes("not found", 'utf-8'))
                return


        if url_path=='/get':
            key=query.split("=")
            try:
                
                values=PRIMARY_KV_STORE[key[1]]
                values=str(values)
                self.wfile.write(bytes(values,"utf-8"))
            except KeyError:
                try:
                    values=BACKUP_KV_STORE[key[1]]
                    values=str(values)
                    self.wfile.write(bytes(values,"utf-8"))
                except KeyError:
                    error="Key not found"
                    self.wfile.write(bytes(error,"utf-8"))

        print_server_data()
        

        

    def do_PUT(self):
        self._set_headers()
        content_length = int(self.headers['Content-Length'])
        put_data = self.rfile.read(content_length)

        data = simplejson.loads(put_data)
        global PRIMARY_RANGE
        global BKP_RANGE
        global PRIMARY_KV_STORE
        global BACKUP_KV_STORE
        global MASTER_ADDR
        key = data['key']
        print("key is", key)
        values = data['values']
        print("values is", values)
        bkp_server_data=[]
        flag=0
        print(MASTER_ADDR)
        
        if PRIMARY_RANGE[0]<=key<=PRIMARY_RANGE[1]:

            print("0")
            if not key in PRIMARY_KV_STORE.keys():
                flag=1
                PRIMARY_KV_STORE[key]=values
                message="SUCCESFULLY WRITTEN"
                self.wfile.write(bytes(message, 'utf-8'))
                bkp_server_data=findBackupNode(key)
                r = requests.put('http://'+str(bkp_server_data[0])+":"+str(bkp_server_data[1])+'/put', data=json.dumps(data))
                print(r.text)
                print("New Primary Data: ",PRIMARY_KV_STORE)
                
            else:
                error="Key already present, Try with a different key"
                self.wfile.write(bytes(error, 'utf-8'))
            

        elif BKP_RANGE[0]<=key<=BKP_RANGE[1]:
            BACKUP_KV_STORE[key]=values
            message="SUCCESFULLY WRITTEN IN BACKUP"
            self.wfile.write(bytes(message, 'utf-8'))
            print("New BackUp Data: ",BACKUP_KV_STORE)

        print_server_data()
        



        
        
def init():
    global node
    global PRIMARY_RANGE
    global BKP_RANGE
    global MASTER_ADDR
    if zk.exists("/kvs/master"):
        l=zk.get_children("/kvs")
        l.sort()
        if node == ("/kvs/" + l[1]) or node == ("/kvs/"+l[2]):
            temp={}
            temp["IP"]=ipaddr
            temp["port"]=port
            node = zk.create("/kvs/slave/slave",bytes(json.dumps(temp), 'utf-8'),ephemeral=True, sequence=True)
            print("Slave created")
            master_data, _ =zk.get("/kvs/master")
            master_data = master_data.decode("utf-8")
            master_data=json.loads(master_data)
            MASTER_ADDR=[master_data["IP"],master_data["port"]]
        else:
            temp={}
            temp["IP"]=ipaddr
            temp["port"]=port
            node = zk.create("/kvs/pool/pool",bytes(json.dumps(temp),'utf-8'),ephemeral=True,sequence=True)
            temp_temp, _ =zk.get("/kvs/temp")
            temp_temp.decode('utf-8')
            if temp_temp=='':
                zk.set("/kvs/temp",bytes(json.dumps(temp),'utf-8'))
            print("Joined Pool")
            master_data, _ =zk.get("/kvs/master")
            master_data = master_data.decode("utf-8")
            master_data=json.loads(master_data)
            MASTER_ADDR=[master_data["IP"],master_data["port"]]
    else:
        
        l=zk.get_children("/kvs")
        l.sort()
        print(node)
        print(l)
        if node=="/kvs/"+l[0]:
            temp={}
            temp["IP"]=ipaddr
            temp["port"]=port
            MASTER_ADDR=[ipaddr,port]
            z_kvs_data, _ = zk.get("/kvs")
            z_kvs_data = z_kvs_data.decode("utf-8")
            z_kvs_data = json.loads(z_kvs_data)
            temp["prim_range"]=z_kvs_data["key_ranges"][0]
            temp["bkp_range"]=z_kvs_data["key_ranges"][1]
            PRIMARY_RANGE=z_kvs_data["key_ranges"][0]
            BKP_RANGE=z_kvs_data["key_ranges"][1]
            z_kvs_data["taken"][0]=1
            zk.set("/kvs",bytes(json.dumps(z_kvs_data),"utf-8"))
            node=zk.create("/kvs/master",bytes(json.dumps(temp),'utf-8'), ephemeral=True)
            print("Master elected")
            if not zk.exists("/kvs/temp"):
                zk.create("/kvs/temp")
            if not zk.exists("/kvs/pool"):
                zk.create("/kvs/pool")
            if not zk.exists("/kvs/cluster"):
                zk.create("/kvs/cluster",bytes("INITIALIZING","utf-8"))
                print("CLUSTER INITIALIZING")
        else:
            time.sleep(3)
            if node == ("/kvs/" + l[1]) or node == ("/kvs/"+l[2]):
                temp={}
                temp["IP"]=ipaddr
                temp["port"]=port
                node = zk.create("/kvs/slave/slave",bytes(json.dumps(temp), 'utf-8'),ephemeral=True, sequence=True)
                print("Slave created")
                master_data, _ =zk.get("/kvs/master")
                master_data = master_data.decode("utf-8")
                master_data=json.loads(master_data)
                MASTER_ADDR=[master_data["IP"],master_data["port"]]
            else:
                temp={}
                temp["IP"]=ipaddr
                temp["port"]=port
                node = zk.create("/kvs/pool/pool",bytes(json.dumps(temp),'utf-8'),ephemeral=True,sequence=True)
                print("Joined Pool")
                master_data, _ =zk.get("/kvs/master")
                master_data = master_data.decode("utf-8")
                master_data=json.loads(master_data)
                MASTER_ADDR=[master_data["IP"],master_data["port"]]
    watcher()


def watcher():
    @zk.ChildrenWatch("/kvs/slave")
    def change_in_slaves(children):
        if node=="/kvs/master":
            global active_slaves
            if active_slaves < len(children):
                print("New Slave Detected.")
                active_slaves+=1
                z_kvs_data, _ =zk.get("/kvs")
                z_kvs_data=z_kvs_data.decode("utf-8")
                z_kvs_data=json.loads(z_kvs_data)
                new_slave="/kvs/slave/"+children[len(children)-1]
                new_slave_data , _ =zk.get(new_slave)
                new_slave_data = new_slave_data.decode("utf-8")
                new_slave_data=json.loads(new_slave_data)
                for i in range(1,3):
                    if z_kvs_data["taken"][i]==0:
                        new_slave_data["prim_range"] = z_kvs_data["key_ranges"][i]
                        new_slave_data["bkp_range"] = z_kvs_data["key_ranges"][i+1 if i<2 else 0]
                        temp={"prim_range_start":new_slave_data["prim_range"][0],"prim_range_end":new_slave_data["prim_range"][1],"bkp_range_start":new_slave_data["bkp_range"][0],"bkp_range_end":new_slave_data["bkp_range"][1]}
                        zk.set(new_slave,bytes(json.dumps(new_slave_data),"utf-8"))
                        z_kvs_data["taken"][i]=1
                        r = requests.get("http://" + new_slave_data["IP"] + ":" + str(new_slave_data["port"]) + "/master/update_meta", params=temp)
                        if r.text=="Done":
                            print("Slave updated with key ranges")
                            zk.set("/kvs",bytes(json.dumps(z_kvs_data),"utf-8"))
                            break
                        else:
                            print("Something is wrong with the slave")
                            break
                


            cluster_status , _ = zk.get("/kvs/cluster")
            cluster_status = cluster_status.decode("utf-8")
            if cluster_status=="INITIALIZING" and active_slaves==2:
                zk.set("/kvs/cluster",bytes("READY","utf-8"))
                print("CLUSTER READY")

        if active_slaves > len(children):
            print("Slaves Death Detected")
            active_slaves-=1
            if(len(children)>0):
                active_slave="/kvs/slave/"+children[0]
                time.sleep(4)
                temp, _ = zk.get(active_slave)
                temp=temp.decode('utf-8')
                temp=json.loads(temp)
                pool_slaves=zk.get_children("/kvs/pool")
                if(len(pool_slaves)<1):
                    print("No Pool slaves available!\nNothing else to do.")
                    return
                pool_slave_data, _ = zk.get("/kvs/pool/"+pool_slaves[0])
                pool_slave_data=pool_slave_data.decode('utf-8')
                pool_slave_data=json.loads(pool_slave_data)
                if temp["prim_range"]==["j","r"]:
                    pool_slave_data["prim_range"]=["s","z"]
                    pool_slave_data["bkp_range"]=["a","i"]
                    zk.set("/kvs/temp",bytes(json.dumps(pool_slave_data),'utf-8'))
                    zk.delete("/kvs/pool/"+pool_slaves[0])
                elif temp["prim_range"]==["s","z"]:
                    pool_slave_data["prim_range"]=["j","r"]
                    pool_slave_data["bkp_range"]=["s","z"]
                    zk.set("/kvs/temp",bytes(json.dumps(pool_slave_data),'utf-8'))
                    zk.delete("/kvs/pool/"+pool_slaves[0])
                print("Slave Death handled")



    @zk.DataWatch("/kvs/temp")
    def change_in_temp(data,stat):
        
        try:
            temp_slave_data=data.decode('utf-8')
            temp_slave_data=json.loads(temp_slave_data)
        except json.decoder.JSONDecodeError:
            return
        if temp_slave_data["IP"]==ipaddr and temp_slave_data["port"]==port:
            global PRIMARY_KV_STORE
            global BACKUP_KV_STORE
            global PRIMARY_RANGE
            global BKP_RANGE
            global node
            prim_data_node=findBackupNode(temp_slave_data["prim_range"][0])
            bkp_data_node=findStorageNode(temp_slave_data["bkp_range"][0])
            PRIMARY_RANGE=temp_slave_data["prim_range"]
            BKP_RANGE=temp_slave_data["bkp_range"]
            print("Getting Primary Data:")
            r=requests.get("http://"+prim_data_node[0]+":"+str(prim_data_node[1])+"/pool_transfer/get_backup")
            temp_prim=r.text
            temp_prim=temp_prim
            PRIMARY_KV_STORE=json.loads(temp_prim)
            print("Got Primary data from Back up server")
            print("Primary data: ",PRIMARY_KV_STORE)
            print("Getting Back Up Data:")
            r=requests.get("http://"+bkp_data_node[0]+":"+str(bkp_data_node[1])+"/pool_transfer/get_prim")
            temp_bkp=r.text
            temp_bkp=temp_bkp
            BACKUP_KV_STORE=json.loads(temp_bkp)
            print("Got Back Up data from Primary server")
            print("BackUp Data: ",BACKUP_KV_STORE)
            node=zk.create("/kvs/slave/slave",bytes(json.dumps(temp_slave_data),'utf-8'),ephemeral=True,sequence=True)
            print("Slave Created")

    @zk.DataWatch("/kvs/cluster")
    def change_in_cluster(data,stat):
        global node
        if data.decode("utf-8")=="READY":
            print("Activating Master Watcher")
            watcher_master()




def watcher_master():
    @zk.DataWatch("/kvs/master")
    def change_in_master(data,stat):
        global node
        if node == "/kvs/master":
            return
        if not 'slave' in node:
            return
        curr_slave_data, _ =zk.get(node)
        curr_slave_data = curr_slave_data.decode("utf-8")
        curr_slave_data=json.loads(curr_slave_data)
        if  data==None and curr_slave_data["bkp_range"][0]=="a" and (not zk.exists("/kvs/master")):
            temp_node=zk.create("/kvs/master",bytes(json.dumps(curr_slave_data),"utf-8"),ephemeral=True)
            print("Master Elected")
            time.sleep(4)
            pool_slaves=zk.get_children("/kvs/pool")
            if(len(pool_slaves)<1):
                print("No Pool slaves available!\nNothing else to do.")
                return
            pool_slave_data, _ =zk.get("/kvs/pool/"+pool_slaves[0])
            pool_slave_data=pool_slave_data.decode("utf-8")
            pool_slave_data=json.loads(pool_slave_data)
            pool_slave_data["prim_range"] = ["a","i"]
            pool_slave_data["bkp_range"] = ["j","r"]
            zk.set("/kvs/temp",bytes(json.dumps(pool_slave_data),'utf-8'))
            zk.delete(node)
            node=temp_node
            zk.delete("/kvs/pool/"+pool_slaves[0])


            


















def update(node):
    if zk.exists(node):
        zk.set(node,b''+str(ipaddr)+"+"+str(port)+"<newrangeofkeys>")
        return True
    else:
        return False
        
def findStorageNode(search):
    global PRIMARY_RANGE
    if PRIMARY_RANGE[0]<=search<=PRIMARY_RANGE[1]:
        return ipaddr,port
    else:
        master_data, _ =zk.get("/kvs/master")
        master_data=master_data.decode("utf-8")
        master_data=json.loads(master_data)
        if master_data["prim_range"][0]<=search<=master_data["prim_range"][1]:
            return master_data["IP"],master_data["port"]
        slave_paths=zk.get_children("/kvs/slave")
        for slave_path in slave_paths:
            slave_data, _  = zk.get("/kvs/slave/"+slave_path)
            slave_data=slave_data.decode("utf-8")
            slave_data=json.loads(slave_data)
            if slave_data["prim_range"][0]<=search<=slave_data["prim_range"][1]:
                return slave_data["IP"],slave_data["port"]

    return -1,-1

def findBackupNode(search):
    global BKP_RANGE
    if BKP_RANGE[0]<=search<=BKP_RANGE[1]:
        return ipaddr,port

    else:
        master_data, _ =zk.get("/kvs/master")
        master_data=master_data.decode("utf-8")
        master_data=json.loads(master_data)
        if master_data["bkp_range"][0]<=search<=master_data["bkp_range"][1]:
            return master_data["IP"],master_data["port"]
        slave_paths=zk.get_children("/kvs/slave")
        for slave_path in slave_paths:
            print("here in find backup 4")
            slave_data, _  = zk.get("/kvs/slave/"+slave_path)
            slave_data=slave_data.decode("utf-8")
            slave_data=json.loads(slave_data)
            if slave_data["bkp_range"][0]<=search<=slave_data["bkp_range"][1]:
                return slave_data["IP"],slave_data["port"]

    return -1,-1



    
        
if __name__ == '__main__':
    from http.server import HTTPServer
    from kazoo.client import KazooClient

    #---------------------------------------------------------
    if len(sys.argv) == 2:
        port=int(sys.argv[1])
    else:
        print("Error: Port number not valid\nUsage: ./server.py <portno>")
        exit()
    hostname = socket.gethostname()    
    #ipaddr = socket.gethostbyname(hostname)             
    ipaddr='127.0.0.1'
    zk = KazooClient(hosts='127.0.0.1:2181')
    #zk = KazooClient(hosts='127.0.0.1:2181')
    zk.start()
    if not zk.exists("/kvs"):
        zk.create("/kvs",bytes('{"key_ranges": [["a", "i"], ["j", "r"], ["s", "z"]], "taken": [0, 0, 0]}','utf-8'))
        print("/kvs created.")
    if not zk.exists("/kvs/slave"):
        zk.create("/kvs/slave")
    node=zk.create("/kvs/active_node",b"junkdata",ephemeral=True,sequence=True)
    print(node)
    init()
    #---------------------------------------------------------
    #host = ipaddr
    host='127.0.0.1'
    server = HTTPServer((host, port), handleRequests)
    server.serve_forever()
