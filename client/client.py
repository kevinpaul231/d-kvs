from urllib.parse import urlparse
from kazoo.client import KazooClient
from kazoo.client import KazooState
import time
import sys
import os
import requests
import json
import sys


#ZK
def my_listener(state):
    if state == KazooState.LOST:
        print("lost")
    elif state == KazooState.SUSPENDED:
        print("suspended")
    else:
        print("connected")
        
#node data is stored in the format data:node ip addr+range of key values
def watcher():
    @zk.DataWatch("/kvs/master")
    def change_in_master(data,stat):
        global master_addr
        global master_port
        if data!=None:
            master_data, _ =zk.get("/kvs/master")
            master_data=master_data.decode("utf-8")
            master_data=json.loads(master_data)
            master_addr=master_data["IP"]
            master_port=master_data["port"]
def masterstat():
    if zk.exists("/master"):
        print("Status :active")
        if len(zk.get_children("/master")) == 0:
            print("No slaves active")
        else:
            print(len(zk.get_children("/master")), "Slaves active")

        data, stat=zk.get("/master")
        print(data.decode("utf-8"), "is the range of values present in master")
         
    else:
        print("node not active")

def onrequest():
    ##client contacts ZK, it returns master addr
    if zk.exists("/kvs/master"):
        print("Status:active")
        data, stat=zk.get("/kvs/master")
        temp=data.decode('utf-8')
        temp=json.loads(temp)
        print('Master server addr: ', temp["IP"],temp["port"])
        return temp["IP"],temp["port"]
    else: 
        print("error: node not active")
        return -1,-1

def contact_master(key, master_addr,master_port):
    ##returns storage node addr where key is present
    payload = {'key':key}
    r = requests.get('http://'+master_addr+':'+str(master_port)+'/search', params=payload)
    
    
    if r.text!="not found":
        temp=r.text
        storage_slave_addr,storage_slave_port = json.loads(temp)
        return storage_slave_addr,storage_slave_port
    else:
        print("Primary server seems to be down.\nLooking for Backup")
        t = requests.get('http://'+master_addr+':'+str(master_port)+'/search_backup', params=payload)
        if t.text!="not found":
            temp=t.text
            storage_slave_addr,storage_slave_port = json.loads(temp)
            print("Back up server found")
            return storage_slave_addr,storage_slave_port
        return -1,-1



#client
def input_handler(input_list): #eg for input_list: 'get abc', 'put abc x y z'
    command = input_list[0]
    
    
    if(command == 'get'): # 'get abc'
        key = input_list[1]

        storage_slave_addr,storage_slave_port = contact_master(key, master_addr,master_port)
        #will return addr of storage node where 'key' is present
        print("Storage slave address is: ", storage_slave_addr,storage_slave_port)
        
        payload = {'key':key}
        r = requests.get('http://'+storage_slave_addr+":"+str(storage_slave_port)+'/get', params=payload)
        values = r.text
        
        print("Record retrieved is:")
        print(key, values)
        
    elif(command == 'put'): # 'put abc x y z'
        key = input_list[1]
        values = []
        
        for i in range(2, len(input_list)):
            values.append(input_list[i])

        storage_slave_addr,storage_slave_port = contact_master(key, master_addr,master_port)
        #will return slave addr where the record will be put

        data = {'key':key, 'values':values}
        r = requests.put('http://'+storage_slave_addr+":"+str(storage_slave_port)+'/put', data=json.dumps(data))

        print(r.text)
        

    elif(command == 'getmultiple'): #'getmultiple abc def ghi'
        keys = []

        for i in range(1, len(input_list)):
            keys.append(input_list[i])
            
        values_list = [] #list of 'values' for each key

        for key in keys:
            storage_slave_addr,storage_slave_port = contact_master(key, master_addr,master_port)
            #values = get(key, storage_slave_addr)
            payload = {'key':key}
            r = requests.get('http://'+storage_slave_addr+":"+str(storage_slave_port)+'/get', params=payload)
            values = r.text
            values_list.append(values)

        i=0
        for key in keys:
            print(key, values_list[i])
            i+=1

    elif(command == 'quit' or command == 'exit'):
        exit(0)
        
    else:
        print('ERROR: Command not found.')

#zookeeper intialisation
zk = KazooClient(hosts='127.0.0.1:2181') ##ZK server ip
#zk = KazooClient(hosts='127.0.0.1:2181') ##ZK server ip
zk.start()
zk.add_listener(my_listener)

#main
while(1):
    cluster_status, _ =zk.get("/kvs/cluster")
    cluster_status=cluster_status.decode("utf-8")
    if cluster_status == "READY":
        break
    else:
        print("Cluster is not ready")


os.system("clear")
print("CLUSTER READY")
watcher()
master_addr,master_port = onrequest()#will return the address of the master node, gives -1 on err 
if master_addr==-1:
    print("Error: Master not found")
    exit()
while(1):
    command_string = input().rstrip()
    input_list = command_string.split(' ')
    input_handler(input_list)
    print("\n")
    
    
    
